package co.edu.uniquindio.arbol;

public class ArbolBinarioNoGenerico {

    private NodoNoGenerico raiz;

    public ArbolBinarioNoGenerico() {
    }

    public void insertar(String key, Integer value) {
        if (raiz != null) {
            raiz.insertar(key, value);
        }
        else {
            raiz = new NodoNoGenerico(key, value);
        }
    }

    public NodoNoGenerico buscar(String key) {
        if (raiz != null) {
            return raiz.buscar(key);
        }
        return null;
    }

    public void eliminar (String key) {
        raiz = eliminar(raiz, key);
    }

    //método interno para realizar la operación
    protected NodoNoGenerico eliminar (NodoNoGenerico subRaiz, String key) {
        if (subRaiz == null) {
            return null;
        }
        else if (key.compareTo(subRaiz.getKey()) < 0) {
            NodoNoGenerico iz;
            iz = eliminar(subRaiz.getIzquierda(), key);
            subRaiz.setIzquierda(iz);
        }
        else if (key.compareTo(subRaiz.getKey()) > 0) {
            NodoNoGenerico dr;
            dr = eliminar(subRaiz.getDerecha(), key);
            subRaiz.setDerecha(dr);
        }
        else {
            NodoNoGenerico q;

            q = subRaiz;
            // nodo a quitar del árbol
            if (q.getIzquierda() == null) {
                subRaiz = q.getDerecha();
            }
            else if (q.getDerecha() == null) {
                subRaiz = q.getIzquierda();
            }
            else {
                // tiene rama izquierda y derecha
                q = reemplazar(q);
            }
            q = null;
        }
        return subRaiz;
    }

    // método interno para susutituir por el mayor de los menores
    private NodoNoGenerico reemplazar(NodoNoGenerico actual) {
        NodoNoGenerico a, p;
        p = actual;
        a = actual.getIzquierda();
        // rama de nodos menores
        while (a.getDerecha() != null)
        {
            p = a;
            a = a.getDerecha();
        }
        actual.setKey(a.getKey());
        actual.setValue(a.getValue());
        if (p == actual)
            p.setIzquierda(a.getIzquierda());
        else
            p.setDerecha(a.getIzquierda());
        return a;
    }

    public String toString() {
        String resultado = "{vacio}";
        if (raiz != null) {
            resultado = raiz.toString(0);
        }

        return resultado;
    }
}
